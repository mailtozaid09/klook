export const media = {
    
    netflixIcon: require('../assets/images/icon.png'),

    netflixText: require('../assets/images/logo.png'),

    comingsoon: require('../assets/images/comingsoon.png'),
    everyonewatching: require('../assets/images/everyonewatching.png'),
    top10: require('../assets/images/top10.png'),

    banner: require('../assets/images/banner.png'),

    close_circle: require('../assets/images/close_circle.png'),
    search: require('../assets/images/search.png'),
    searchIcon: require('../assets/images/searchIcon.png'),
    cast: require('../assets/images/cast.png'),


    remind: require('../assets/images/remind.png'),
    info: require('../assets/images/info.png'),

    play: require('../assets/images/play.png'),
    play_circle: require('../assets/images/play_circle.png'),
    
    like: require('../assets/images/like.png'),
    share: require('../assets/images/share.png'),
    plus: require('../assets/images/plus.png'),

    edit: require('../assets/images/edit.png'),

    down_arrow: require('../assets/images/down_arrow.png'),
    right_arrow: require('../assets/images/right_arrow.png'),



    onboarding1: require('../assets/images/onboarding/onboarding1.png'),
    onboarding2: require('../assets/images/onboarding/onboarding2.png'),
    onboarding3: require('../assets/images/onboarding/onboarding3.png'),
    onboarding4: require('../assets/images/onboarding/onboarding4.png'),
    
    
    home: require('../assets/images/tabicon/home.png'),
    home_fill: require('../assets/images/tabicon/home_fill.png'),
    destinations: require('../assets/images/tabicon/destinations.png'),
    destinations_fill: require('../assets/images/tabicon/destinations_fill.png'),
    deals: require('../assets/images/tabicon/deals.png'),
    deals_fill: require('../assets/images/tabicon/deals_fill.png'),
    trips: require('../assets/images/tabicon/trips.png'),
    trips_fill: require('../assets/images/tabicon/trips_fill.png'),
    user: require('../assets/images/tabicon/account.png'),
    user_fill: require('../assets/images/tabicon/account_fill.png'),


    delete: require('../assets/images/delete.png'),
    account: require('../assets/images/account.png'),
    settings: require('../assets/images/settings.png'),
    mylist: require('../assets/images/mylist.png'),

    gamehandle: require('../assets/images/gamehandle.png'),
    language: require('../assets/images/language.png'),
    maturity: require('../assets/images/maturity.png'),
    subtitle: require('../assets/images/subtitle.png'),
    autoplay: require('../assets/images/autoplay.png'),
    autopreview: require('../assets/images/autopreview.png'),

    kids: require('../assets/images/profiles/kids.png'),
    
    profile1: require('../assets/images/profiles/profile1.png'),
    profile2: require('../assets/images/profiles/profile2.png'),
    profile3: require('../assets/images/profiles/profile3.png'),

}
