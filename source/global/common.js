import AsyncStorage from "@react-native-async-storage/async-storage";

export const orderId = () => {

    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
  
    return (
      S4() +  S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4()
    );
}

export const productId = () => {
  return Math.floor(100000 + Math.random()*(999999 - 100000 + 1))
}   
