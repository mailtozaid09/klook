export const colors = {
    accent: '#211f1f',

    dark: '#121212',
    red: '#e50914',
    light: '#fff',
    grey: '#8c8c8c',


    primary: '#ed6730',
    primary2: '#D22F26',
    primary3: '#DF7375',

    black: '#000',

    light_red: '#FEA7A9',

    dark_blue: '#212c46',
    
   
    white: '#fdfdfd',
    
    dark_gray: '#828282',
    dark_gray2: '#323232',
    dark_gray3: '#191919',

    light_gray: '#e6e6e6',
    light_gray2: '#cbcbcb',
    light_gray3: '#b2b2b2',
}