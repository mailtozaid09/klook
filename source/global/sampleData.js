import { colors } from "./colors";
import { media } from "./media";

export const users = [
  {
    id: 3,
    name: 'Zaid',
    email: 'zaid',
    phone: '55997386673',
    address: 'Rionegro, Colômbia',
    password: 'zaid',
  },
  {
    id: 1,
    name: 'Pablo Escobar',
    email: 'pablo@escobar.com',
    phone: '55997386673',
    address: 'Rionegro, Colômbia',
    password: 'pablo123',
  },
  {
    id: 2,
    name: 'Michael Scofield',
    email: 'michael@scofield.com',
    phone: '55992526914',
    address: 'Oxfordshire, Inglaterra',
    password: 'michael123',
  },
];


export const movies = [
    {
      id: 1,
      title: 'Stranger Things',
      image: require('../assets/images/movies/strangerthings.jpg'),
    },
    {
      id: 2,
      title: 'Deadpool 2',
      image: require('../assets/images/movies/deadpool.jpg'),
    },
    {
      id: 3,
      title: 'Mean Girls',
      image: require('../assets/images/movies/meangirls.jpg'),
    },
    {
      id: 4,
      title: 'Moonlight',
      image: require('../assets/images/movies/moonlight.jpg'),
    },
    {
      id: 5,
      title: 'Mr Bean',
      image: require('../assets/images/movies/mrbean.jpg'),
    },
    {
      id: 6,
      title: 'Whitehouse',
      image: require('../assets/images/movies/whitehouse.jpg'),
    },
    {
      id: 7,
      title: 'Jaws',
      image: require('../assets/images/movies/jaws.jpg'),
    },
  ];

export const onboarding_data = [
    // {
    //     id: 1,
    //     image: media.onboarding1,
    //     title: 'A world of possibilities',
    //     description: 'Whatever brings you joy - and no matter where - Klook takes you to it.',
    //     bg_color: '#eb662f',  
    // },
    // {
    //     id: 2,
    //     image: media.onboarding2,
    //     title: 'Wait less explore more',
    //     description: 'Get the good stuff faster with instant confirmation and e-voucher entry.',
    //     bg_color: '#4a41c0',  
    // },
    // {
    //     id: 3,
    //     image: media.onboarding3,
    //     title: "Find deals you'll love",
    //     description: "Who doesn't love a good deal? We'll show you quality experiences at great prices.",
    //     bg_color: '#eb662f',  
    // },
    {
      id: 4,
      image: media.onboarding4,
      title: 'One last step...',
      description: "To get the most joy out of Klook, you'll need to allow the following:",
      bg_color: '#4a41c0',  
      options: [
        {
          id: 1,
          option_image: media.home_fill,
          option_title: 'Location',
          option_description: 'Share your location to get nearby recommendations',
        },
        {
          id: 2,
          option_image: media.home_fill,
          option_title: 'Notifications',
          option_description: 'Find out about any updates to your bookings and exclusive deals',
        },
      ]
  },
]


export const newsAndHotTabs = [
  {
      id: 1,
      image: media.comingsoon,
      title: 'Coming Soon',
     
  },
  {
      id: 2,
      image: media.everyonewatching,
      title: "Everyone's Watching",
   
  },
  {
      id: 3,
      image: media.top10,
      title: 'Top 10',
  },
]