const INITIAL_STATE = {
  loader: false
};

export default function home(state = INITIAL_STATE, action) {
  switch (action.type) {
    // case '@auth/SIGN_IN':
    //   return {
    //     ...state,
    //     user: action.payload.user,
    //   };
    // case '@auth/SIGN_OUT':
    //   return { ...state, user: null };
    
      case '@home/LOADER':
      return {
        ...state,
        loader: action.payload.loader,
    };
    default:
      return state;
  }
}
