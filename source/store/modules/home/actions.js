import { Alert } from 'react-native';
import { users } from '../../../global/sampleData';
import AsyncStorage from '@react-native-async-storage/async-storage';

export function screenLoader(loader) {
  return {
    type: '@home/LOADER',
    payload: {
      loader,
    },
  };
}
