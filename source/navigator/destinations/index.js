import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import DestinationsScreen from '../../screens/dashboard/destinations';


const Stack = createStackNavigator();


const DestinationsStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Destinations" 
        >
            <Stack.Screen
                name="Destinations"
                component={DestinationsScreen}
                options={{
                    headerShown: true,
                    headerTitle: 'Destinations',
                    // headerLeft: () => null,
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
        
        </Stack.Navigator>
    );
}

export default DestinationsStack