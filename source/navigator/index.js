import React, {useState, useEffect} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { useSelector } from 'react-redux';

import MainNavigator from './MainNavigator';
import Loader from '../components/loader';
import { screenHeight, screenWidth } from '../global/constants';

const Navigator = ({navgation}) => {

    const [userDetails, setUserDetails] = useState({});

    const user = useSelector(state => state.auth.user);

    const loader = useSelector(state => state.home.loader);


    useEffect(() => {
        //getUserDetails()
    }, [])
    


    return (
        <>
            <MainNavigator />

            {loader &&  
            <View style={{position: 'absolute', top: 0,  height: screenHeight, width: screenWidth,}} >
                <Loader />
            </View>
            }
        </>
    );
}

export default Navigator