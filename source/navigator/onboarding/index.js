import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import OnBoardingScreen from '../../screens/onboarding';
import Header from '../../components/header/onboarding';
import { colors } from '../../global/colors';


const Stack = createStackNavigator();


const OnboardingStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="OnBoarding" 
        >
            <Stack.Screen
                name="OnBoarding"
                component={OnBoardingScreen}
                options={({navigation}) => ({
                    headerShown: false,
                    // headerStyle: styles.headerStyle,
                    // headerTitle: '',
                    // headerTitle: () => (
                    //     <Header navigation={navigation} />
                    // ),
                })}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: colors.black,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
    }
})

export default OnboardingStack