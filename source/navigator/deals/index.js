import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import DealsScreen from '../../screens/dashboard/deals';


const Stack = createStackNavigator();


const DealsStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Deals" 
        >
            <Stack.Screen
                name="Deals"
                component={DealsScreen}
                options={{
                    headerShown: true,
                    headerTitle: 'Deals',
                    // headerLeft: () => null,
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
        
        </Stack.Navigator>
    );
}

export default DealsStack