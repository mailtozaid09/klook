import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import TripsScreen from '../../screens/dashboard/trips';


const Stack = createStackNavigator();


const TripsStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Trips" 
        >
            <Stack.Screen
                name="Trips"
                component={TripsScreen}
                options={{
                    headerShown: true,
                    headerTitle: 'Trips',
                    // headerLeft: () => null,
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
        
        </Stack.Navigator>
    );
}

export default TripsStack