import React, {useState, useEffect} from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, SafeAreaView } from 'react-native'

import LoginButton from '../../../components/button';


import { colors } from '../../../global/colors';
import Input from '../../../components/input';
import { InstagramSans } from '../../../global/fontFamily';



const CreateAccount = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);

    const continueFunction = () => {
        setIsLoading(true)

        setTimeout(() => {
            setIsLoading(false)
            navigation.navigate('AccountCreated')
        }, 1000);
    }

    return (
        <SafeAreaView style={styles.container} >
            <View style={{width: '100%', padding: 20, paddingHorizontal: 30}} >
                <Text style={styles.title} >Ready to experience unlimited TV shows & movies?</Text>
                <Text style={styles.subtitle} >Create an account to learn more about Netflix.</Text>
                
                <Input
                    title="Email"
                    placeholder="Enter your email"
                    inputColor={colors.white}
                />

                <Input
                    title="Password"
                    placeholder="Enter your password"
                    inputColor={colors.white}
                />


                <LoginButton
                    title="CONTINUE" 
                    loading={isLoading}
                    onPress={() => {continueFunction()}}
                />
            </View>

            

            
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.white
    },
    title: {
        fontSize: 24,
        color: colors.black,
        textAlign: 'center',
        fontFamily: InstagramSans.Bold,
    },
    subtitle: {
        fontSize: 20,
        color: colors.grey,
        fontFamily: InstagramSans.Medium,
        textAlign: 'center',
        marginVertical: 25,
    },
})

export default CreateAccount