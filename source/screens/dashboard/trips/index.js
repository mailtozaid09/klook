import React, {useState, useEffect} from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native'

const TripsScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <Text>TripsScreen</Text>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    }
})

export default TripsScreen
