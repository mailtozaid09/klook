import React, {useState, useEffect} from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native'

const DestinationsScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <Text>DestinationsScreen</Text>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    }
})

export default DestinationsScreen
