import React, { useState, useEffect } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
 
import Swiper from 'react-native-swiper'

import { onboarding_data } from '../../global/sampleData'
import { colors } from '../../global/colors'
import LoginButton from '../../components/button'
import { InstagramSans } from '../../global/fontFamily'
import { useDispatch, useSelector } from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Loader from '../../components/loader'
import { screenLoader } from '../../store/modules/home/actions'
import { screenWidth } from '../../global/constants'


const OnBoardingScreen = ({navigation}) => {

    const dispatch = useDispatch()

    const [userDetails, setUserDetails] = useState({});

    const user = useSelector(state => state.auth.user);

    const loader = useSelector(state => state.home.loader);

    const current_profile = useSelector(state => state.profile.current_profile);
    const profile_list = useSelector(state => state.profile.profile_list);

    //console.log("current_profile ", current_profile);

    //console.log("user ", user);

    console.log("profile_list ", profile_list);

    
    useEffect(() => {
        dispatch(screenLoader(true))

        getUserDetails()
    }, [user])
    

    const getUserDetails = async () => {
        let user = await AsyncStorage.getItem('user_details');  
        let user_details = JSON.parse(user);  
        setUserDetails(user_details)

        dispatch(screenLoader(false))
        // if(user != null){
        //     navigation.navigate('WhoIsWatching')
        //     setTimeout(() => {
        //         dispatch(screenLoader(false))
        //     }, 1000);
        // }else{
        //     setTimeout(() => {
        //         dispatch(screenLoader(false))
        //     }, 1000);
            
        //     navigation.navigate('OnboardingStack', {screen:'OnBoarding'})
        // }
    }
   


    return (
        <>
            {loader
                ?
                <Loader />
                :
                <>
                <Swiper 
                loop={false}
                style={styles.wrapper} 
                showsButtons={false}
                dotColor={colors.grey}
                activeDotColor={colors.primary}
                paginationStyle={{ position: 'absolute', bottom: 120}}
            >
            {onboarding_data.map((item, index) => (
                <View 
                    key={index}
                    style={[styles.itemContainer, {backgroundColor: item.bg_color}]}
                >
                    

                    {index == 1
                    ?
                    <View style={[styles.itemContainer, {paddingBottom: 150}]} >
                        <View style={[ {width: '100%', paddingHorizontal: 20, backgroundColor: 'red'}]} >
                            <View style={{width: '70%'}} >
                                <Text style={styles.text}>{item.title}</Text>
                            </View>
                            <Text style={styles.description}>{item.description}</Text>
                        </View>

                        <Image source={item.image} style={{height: screenWidth*0.75, width: screenWidth*0.75, marginTop: 50,}} />
                    </View>
                    :
                    index == 0
                    ? 
                    <View style={{backgroundColor: 'red', alignItems: 'center', width: '100%', flex: 1}} >
                        <Image source={item.image} style={{height: screenWidth*0.3, width: screenWidth*0.3, marginVertical: 20}} />
                        
                        <View style={[ {width: '100%', marginTop: 20, paddingHorizontal: 20, backgroundColor: 'red'}]} >
                            <Text style={styles.text}>{item.title}</Text>
                            <Text style={styles.description}>{item.description}</Text>
                        </View>


                        <View style={{flex: 1, width: '100%'}} >
                            {item?.options?.map((options) => (
                                <View style={{flexDirection: 'row', paddingHorizontal: 20}} >
                                    <Image source={options.option_image} style={{height: 28, width: 28,}} />

                                    <View style={{flex: 1}} >  
                                        <Text style={styles.text}>{options.option_title}</Text>
                                        <Text style={styles.description}>{options.option_description}</Text>
                                    </View>
                                </View>
                            ))}
                        </View>
                    </View>
                    :
                    <View style={[styles.itemContainer, {paddingBottom: 150}]} >
                        <Image source={item.image} style={{height: screenWidth*0.75, width: screenWidth*0.75, marginVertical: 20}} />
                        
                        <View style={[ {width: '100%', marginTop: 20, paddingHorizontal: 20, backgroundColor: 'red'}]} >
                            <Text style={styles.text}>{item.title}</Text>
                            <Text style={styles.description}>{item.description}</Text>
                        </View>

                    </View>
                    }
                    
                </View>
            ))}
            </Swiper>
            {/* <View style={{width: '100%', position: 'absolute', bottom: 25, padding: 20}} >
               <LoginButton
                    title="GET STARTED" 
                    onPress={() => navigation.navigate('LoginStack', {screen: 'SignUp'})}
                />
            </View> */}
            </>
            }
        </>
    )
}


const styles = StyleSheet.create({
    wrapper: {},
    itemContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 50,
    },
    text: {
        color: '#fff',
        fontSize: 30,
        marginBottom: 20,
        fontFamily: InstagramSans.Bold,
    },
    description: {
        color: '#fff',
        fontSize: 20,
        fontFamily: InstagramSans.Regular,
    }
  })
   
export default OnBoardingScreen