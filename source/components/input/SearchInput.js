import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet, TextInput } from 'react-native'
import { colors } from '../../global/colors'
import { InstagramSans } from '../../global/fontFamily'
import { media } from '../../global/media'



const SearchInput = ({ placeholder, searchValue, clearInputValue, onChangeText, inputColor}) => {
    return (
        <View>
           
            <View style={styles.container} >
                <Image source={media.searchIcon} style={{height: 28, width: 28, marginRight: 15}} />
                <TextInput
                    autoCapitalize="none"
                    autoCorrect={false}
                    value={searchValue}
                    style={[styles.inputStyle, {color: inputColor ? colors.black : colors.white} ]}
                    placeholder={placeholder}
                    onChangeText={onChangeText}
                    placeholderTextColor={colors.grey}
                />
                {searchValue && <TouchableOpacity
                    onPress={clearInputValue}
                >
                    <Image source={media.close_circle} style={{height: 28, width: 28, }} />
                </TouchableOpacity>}
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        marginVertical: 20,
        marginTop: 4,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.accent,
        justifyContent: 'space-between',
    },
    inputStyle: {
        flex: 1,
        height: 60,
        marginRight: 20,
        fontSize: 16,
        fontFamily: InstagramSans.Medium
    },
    title: {
        fontSize: 16,
        color: colors.black,
        marginBottom: 8,
        fontFamily: InstagramSans.Bold
    }
})

export default SearchInput