import React from 'react';
import { Text, View, ScrollView, StyleSheet, TouchableOpacity, Image, } from 'react-native';
import { colors } from '../../global/colors';
import { InstagramSans } from '../../global/fontFamily';


const MoviesList = ({ title, movies, navigation }) => {
  return (
    <View>
      <Text style={styles.title} >{title}</Text>

      <ScrollView horizontal showsHorizontalScrollIndicator={false} keyboardShouldPersistTaps="always" contentContainerStyle={{padding: 20, paddingLeft: 0, paddingTop: 0}} >
        {movies.map(movie => (
          <TouchableOpacity
            key={movie.id}
            activeOpacity={0.6}
            onPress={() => navigation.navigate('MoviesDetails', { movie })}
          >
             <Image
              style={{height: 180, width: 110, marginRight: 10, borderRadius: 8, }}
              source={{
                uri: `https://image.tmdb.org/t/p/original${movie?.poster_path}`
              }}
            />
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        color: colors.light,
        marginBottom: 10,
        fontFamily: InstagramSans.Bold,
    },
    imageCover: {
        height: 175,
        width: 125,
        marginRight: 8,
        borderRadius: 8,
    }
})



export default MoviesList;
