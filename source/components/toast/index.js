import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet, TextInput } from 'react-native'
import { colors } from '../../global/colors'
import { InstagramSans } from '../../global/fontFamily'

import CheckIcon from 'react-native-vector-icons/AntDesign';

const ToastMessage = ({title,}) => {
    return (
        <View style={styles.container} >
            <CheckIcon name="checkcircle" style={{marginHorizontal: 20}} size={28} />
            <Text style={styles.title} >{title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#8dbf8e',
        height: 60,
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 4,
        marginBottom: 30,
    },
    title: {
        fontSize: 18,
        color: colors.black,
        fontFamily: InstagramSans.Medium
    }
})

export default ToastMessage