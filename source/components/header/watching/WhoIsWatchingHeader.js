import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native'

import { media } from '../../../global/media'

import BackArrow from 'react-native-vector-icons/Feather';
import { colors } from '../../../global/colors';
import { InstagramSans } from '../../../global/fontFamily';

const WhoIsWatchingHeader = ({navigation, editMode, onEditFunction}) => {
    return (
        <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 10, marginBottom: 30}} >
            <Text> </Text>
            <Text style={styles.title} >{editMode == false ? "Who's Watching?" : "Manage Profiles" }</Text>
            
            <TouchableOpacity
                    onPress={onEditFunction}
                >
                <Text style={styles.edit} >{editMode == false ? 'Edit' : 'Done'}</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        fontSize: 22,
        color: colors.white,
        fontFamily: InstagramSans.Medium
    },
    edit: {
        fontSize: 18,
        color: colors.white,
        fontFamily: InstagramSans.Regular
    }
})

export default WhoIsWatchingHeader