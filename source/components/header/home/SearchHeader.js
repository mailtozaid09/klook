import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native'

import { media } from '../../../global/media'

import BackArrow from 'react-native-vector-icons/Feather';
import { colors } from '../../../global/colors';

const SearchHeader = ({navigation}) => {
    return (
        <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 0,}} >
            <TouchableOpacity
                    onPress={() => {navigation.goBack()}}
                >
                    <BackArrow name="arrow-left" size={28}  color={colors.white} />
                </TouchableOpacity>
            <View>
                <Image source={media.netflixText} style={{height: 35, width: 120, resizeMode: 'contain'}} />
            </View>
        </View>
    )
}

export default SearchHeader