import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native'

import { media } from '../../../global/media'
import { colors } from '../../../global/colors'
import { InstagramSans } from '../../../global/fontFamily'



const CreateAccountHeader = ({navigation, isAccountCreated}) => {
    return (
        <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 0,}} >
            <View>
            <Image source={media.netflixText} style={{height: 35, width: 120, resizeMode: 'contain'}} />
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', }} >
                <TouchableOpacity
                    onPress={() => {}}
                >
                    <Text style={styles.helpText} >HELP</Text>
                </TouchableOpacity>
                {isAccountCreated
                ?
                <TouchableOpacity
                    onPress={() => {navigation.goBack()}}
                >
                    <Text style={[styles.helpText, {marginLeft: 20}]} >SIGN OUT</Text>
                </TouchableOpacity>
                :
                <TouchableOpacity
                    onPress={() => {navigation.navigate('LoginStack', {screen: 'Login'})}}
                >
                    <Text style={[styles.helpText, {marginLeft: 20}]} >SIGN IN</Text>
                </TouchableOpacity>
                }

                

            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    helpText: {
        fontSize: 14, 
        color: colors.black,
        fontFamily: InstagramSans.Medium,
    }
})

export default CreateAccountHeader